# shell-administration-scripts

This is a collection of commands that I need again and again and therefore have summarized in a small script.

## General

### Branches
* **stable** is the current stable build
* **develop** is the development build in which new features are implemented and tested
* **old-stable** is the original/old script 

### Functions
* Nextcloud
	* Update
	* App Update
	* Maintenance ON
	* Maintenance OFF
	* Readout deleted LDAP User
	* Backup deleted LDAP User
	* Remove deleted LDAP User
	* Remove  User
	* Last seen
	* Add missing indices
	* BigInit identifiers
	* Scan
	* CleanUp
	* User report
	* LDAP Search
	* Custom Command
* Update phpMyAdmin
* Update MediaWiki
* Update Collabora (requires docker)
* Update BookStack (requires git & composer)
* Update Friendica (requires git & composer)
* Update FreshRSS (requires git)
* Update Framadate
* Backup


## Usage
1) Clone the repository
2) join the repo `cd shell-administration-script`
3) Give the "manage" file execute rights `chmod +x manage`
4) Run the script `./manage` and answer the questions
5) edit the config created by the script `nano config`
6) if you answered yes to the second question you can run the script from anywhere with `manage` otherwise you have to enter the path and execute the script `bash /pathto/shell-administration-script/manage`

### Update
Updates are downloaded automatically, this can be disabled by setting `SELFUPDATE=false` in config. After each update the manage must be made executable again if you dont use the symlink `chmod +x /pathto/shell-administration-script/manage`. If something has been changed in the config, you will be notified when you start the program.

### create symlink
If you did not create a symlink during the initial setup, you can do this by starting the script with the symlink switch `manage symlink`. To remove the symlink execute the command again.

### Backup
Require [borgbackup](https://www.borgbackup.org/)
To use the function you must first [initializes](https://borgbackup.readthedocs.io/en/stable/usage/init.html) a backup repository.
Start the script with the backup switch `manage backup`

### Debug
To activate the debug mode use the `manage debug` button, but the db passwords will be displayed in the console as well.

## Extensions/improvements
Create a report [here](https://codeberg.org/Tealk/shell-administration-script/issues) and surprise me with your ideas.

## Bug Report
Feel free to create a report [here](https://codeberg.org/Tealk/shell-administration-script/issues)
