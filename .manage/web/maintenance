#!/bin/bash
# Author:  Tealk
# Repo: https://codeberg.org/Tealk/shell-administration-script

# shellcheck disable=SC2034

if [ "${PROGNAME}" = "NextCloud" ]; then
	if [ "$1" = "on" ]; then
		title="Enable maintenance mode"
		printf "  %b %s..." "${INFO}" "${title}"

		if [ -d "${FULLPATH}" ]; then
			if ! sudo -u "${WEBUSER}" "${PHP}" "${FULLPATH}"/occ maintenance:mode --on >/dev/null 2>&1; then
				reason="can't enter maintenance mode because $?"
				ERROR
			else
				printf "%b  %b %s\\n" "${OVER}" "${TICK}" "${title}"
			fi
		else
			reason="can't find ${FULLPATH}"
			ERROR
		fi
	fi

	if [ "$1" = "off" ]; then
		title="Disable maintenance mode"
		printf "  %b %s..." "${INFO}" "${title}"

		if [ -d "${FULLPATH}" ]; then
			if ! sudo -u "${WEBUSER}" "${PHP}" "${FULLPATH}"/occ maintenance:mode --off >/dev/null 2>&1; then
				reason="can't disable maintenance mode because $?"
				ERROR
			else
				printf "%b  %b %s\\n" "${OVER}" "${TICK}" "${title}"
			fi
		else
			reason="can't find ${FULLPATH}"
			ERROR
		fi
	fi

	if [ "$1" = "indices" ]; then
		title="Add missing indices"
		printf "  %b %s..." "${INFO}" "${title}"

		if [ -d "${FULLPATH}" ]; then
			if ! sudo -u "$WEBUSER" "$PHP" "$FULLPATH"/occ db:add-missing-indices >/dev/null 2>&1; then
				reason="can't add missing indices"
				ERROR
			else
				printf "%b  %b %s\\n" "${OVER}" "${TICK}" "${title}"
			fi
		else
			reason="can't find ${FULLPATH}"
			ERROR
		fi
	fi
fi
if [ "${PROGNAME}" = "writefreely" ]; then
	if [ "$1" = "on" ]; then
		title="Stop ${PROGNAME}"
		printf "  %b %s..." "${INFO}" "${title}"

		if [ -d "${FULLPATH}" ]; then
			if ! systemctl stop writefreely >/dev/null 2>&1; then
				reason="can't stop because $?"
				ERROR
			else
				printf "%b  %b %s\\n" "${OVER}" "${TICK}" "${title}"
			fi
		else
			reason="can't find ${FULLPATH}"
			ERROR
		fi
	fi

	if [ "$1" = "off" ]; then
		title="Start ${PROGNAME}"
		printf "  %b %s..." "${INFO}" "${title}"

		if [ -d "${FULLPATH}" ]; then
			if ! systemctl start writefreely >/dev/null 2>&1; then
				reason="can't start because $?"
				ERROR
			else
				printf "%b  %b %s\\n" "${OVER}" "${TICK}" "${title}"
			fi
		else
			reason="can't find ${FULLPATH}"
			ERROR
		fi
	fi
fi